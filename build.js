const fs = require('fs');
const CleanCSS = require('clean-css');
const path = require('path');

const isFile = source => fs.statSync(source).isFile();
const isDirectory = source => fs.statSync(source).isDirectory();
const readDirectories = (source, callback) => fs.readdir(source, (err, list) => {
    callback(err, list.map(name => path.join(source, name)).filter(isDirectory));
});
const readFiles = (source, callback) => fs.readdir(source, (err, list) => {
    callback(err, list.map(name => path.join(source, name)).filter(isFile));
});
const getDirectories = (source) => fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);
const getFiles = (source) => fs.readdirSync(source).map(name => path.join(source, name)).filter(isFile);
const naturalSort = (arr) => {
    var collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
    return arr.sort(collator.compare);
}
const minifyCache = {};
const readAndMinify = (filepath) => {
    if (filepath in minifyCache) {
        return minifyCache[filepath];
    }
    var input = fs.readFileSync(filepath, "utf8");
    var output = new CleanCSS({
        level: 2,
    }).minify(input);
    return minifyCache[filepath] = output;
};
const writeOutputFile = (filepath, contents, success, failure) => {
    fs.truncate(filepath, 0, () => {
        fs.writeFile(filepath, contents, 'utf8', (err) => {
            if (err) {
                if (failure) failure("Failed to write output file (" + filepath + ") " + err);
            } else {
                if (success) success();
            }
        });
    });
};

(function() {
    console.log('Minifying...');

    function packOutput(pkg_name, files, reject) {
        var out = {
            stats: { originalSize: 0, minifiedSize: 0 },
            styles: "/* " + pkg_name + " by /u/kwwxis */\n\n" +
                "/* !!!!!!!!!! DO NOT EDIT THE STYLESHEET HERE. " +
                "USE https://destinyreddit.com/admin/stylesheet !!!!!!!!!! */\n\n",
        };
        files.forEach((f) => {
            let res = readAndMinify(f);

            if (res.errors.length)
                reject(output.errors);
            else if (res.warnings.length)
                reject(output.warnings);

            out.styles += res.styles;
            out.stats.originalSize += res.stats.originalSize;
            out.stats.minifiedSize += res.stats.minifiedSize;
        });
        return out;
    }

    function processModDir(dirname, resolve, reject) {
        let names = {
            pkg_name: path.basename(dirname),
            mod_dir: dirname,
            mod_files: naturalSort(getFiles(dirname).filter((f) => f.endsWith('.css'))),
            base_file: './base.css',
            dist_file: './dist/' + path.basename(dirname) + '.css',
        };

        let res = packOutput(names.pkg_name, [names.base_file].concat(names.mod_files), reject);

        console.log(names.pkg_name + ':');
        console.log('  Original file size: ' + res.stats.originalSize + ' bytes');
        console.log('  Minified file size: ' + res.stats.minifiedSize + ' bytes');

        writeOutputFile(names.dist_file, res.styles, resolve, reject);
    }

    readDirectories('./mod', (err, dirs) => {
        let promises = dirs.map((dirname) => {
            return new Promise((resolve, reject) => {
                processModDir(dirname, resolve, reject);
            });
        });
        Promise.all(promises).then(
            () => console.log('Done!'),
            (reason) => console.log(reason)
        );
    });
})();