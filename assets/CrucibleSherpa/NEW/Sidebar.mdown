Welcome to **[Crucible Sherpa](/r/CrucibleSherpa)**

>* **Useful Links**
 * [Bungie Weekly Update](http://goo.gl/l6TxoV)
 * [Crucible Maps](https://www.reddit.com/r/CrucibleSherpa/wiki/maps)
 * [CrucibleSherpa Guides](https://www.reddit.com/r/CrucibleSherpa/wiki/guides)
 * [Destiny Dictionary](http://redd.it/35n9tn)
 * [How to be a Sherpee and get the most of your time](http://redd.it/3rmos4)
>* **Sherpa Cards**
 * [Verified Sherpas](https://www.reddit.com/r/CrucibleSherpa/wiki/sherpacards/verifiedsherpas)
 * [PS4 Sherpas](https://www.reddit.com/r/CrucibleSherpa/wiki/sherpacards/ps4sherpacards)
 * [PS3 Sherpas](https://www.reddit.com/r/CrucibleSherpa/wiki/sherpacards/ps3sherpacards)
 * [Xbox One Sherpas](https://www.reddit.com/r/CrucibleSherpa/wiki/sherpacards/xb1sherpacards)
 * [Xbox 360 Sherpas](https://www.reddit.com/r/CrucibleSherpa/wiki/sherpacards/xb360sherpacards)
 * [How to make your very own Sherpa card](https://www.reddit.com/r/CrucibleSherpa/wiki/scip)


######**[Welcome to /r/CrucibleSherpa!](https://redd.it/3kezqh) | [How to become a Sherpee](https://redd.it/3rmos4) | [Sherpee Etiquette](https://redd.it/3wuudq/) | [Sherpa Awards](https://www.reddit.com/r/CrucibleSherpa/wiki/sherpaawards) | [Paid Services Notice 03/19/2017](https://redd.it/60blq7)**

## **Community Links**

* [Sherpa List](https://goo.gl/vyBCNC)
* [Twitch Streams](https://goo.gl/4Pt75T)
* [Destiny The Game](https://goo.gl/9b3xGf)
* [Fireteams](http://goo.gl/UvisUG)
* [Destiny Sherpas](http://goo.gl/OtoFXb)
* [Destiny Tech Support](https://goo.gl/iaaibC)
* [Destiny Discord](https://discord.gg/DestinyReddit)
* [Bungie](http://www.bungie.net/)

----

[**Dark Mode** *The Darkness Consumed You*](https://dm.reddit.com/r/CrucibleSherpa "Switch mode")

[**Light Mode** *Become a Guardian of the Light*](https://www.reddit.com/r/CrucibleSherpa "Switch mode")

---

# [Farm Rules](https://goo.gl/11Q9Nj)

1. Follow the [Reddiquette](http://www.reddit.com/wiki/reddiquette) when submitting and commenting. Keep it civil and do not make personal attacks or use offensive language in addressing others. Absolutely no harassment, witchhunting, sexism, racism or hate speech will be tolerated.

2. **We do not allow pure LFG posts nor posts explicitly asking for (OR OFFERING) carries on this site.  Any guardians simply looking for a skilled player to help them with Trials should post in our sticked Megathreads.**  Any LFS posts need to include details of what you would like to learn in the Crucible. This sub exists mainly for the purpose of finding or requesting a Crucible Sherpa to help improve your skills and expand your pvp knowledge. If you are looking for a group of equally skilled guardians, head on over to /r/Fireteams

3. Users may not offer material incentives to encourage receiving help.  **No advertising, selling, trying to buy, trading, or begging.**

4. **Twitch streams are allowed on an educational basis**. You are welcome to link your twitch in an LTS post, so long as you give priority to those coming from the sub. Absolutely no carries will be allowed if you are advertising. Check our [Twitch Streams](/r/CrucibleSherpa/wiki/twitch) for a list of the streamers currently online.

5. **Use the flair system when posting.** Ask a mod if you are unsure of how to add a flair or which flair would be appropriate. Posts without flair will be removed within a reasonable time after posting.

6. If you want to become a Crucible Sherpa, we recommend you post a [Sherpa Card](https://www.reddit.com/r/CrucibleSherpa/wiki/scip). Gathering feedback on your Sherpa Card allows you to work towards earning [Verified Status](https://www.reddit.com/r/CrucibleSherpa/wiki/verified) and the perks that come with it. 

7. **Sherpa card comments are for feedback only.**  Please see [this post](https://www.reddit.com/r/CrucibleSherpa/comments/3s4x4y/new_sherpa_card_comment_rule/) or message a mod for further information.  All non feedback comments on a sherpa card will be removed going forward.

8. This sub is not to be used for **any form of recruitment**. Any links to outside source sites, including other LFG sites, clan pages, etc., will be removed. [Message the mod team](https://www.reddit.com/message/compose?to=%2Fr%2FCrucibleSherpa) before posting such links.

**[Full Subreddit Rules](/r/CrucibleSherpa/w/rules)**

---

> ##Filters
[](https://goo.gl/VWne8S)| PS4 LTS | [](https://goo.gl/Sx9M6j)| PS4 LFS
-|-|-|-
[](https://goo.gl/5uEQdp)| XB1 LTS | [](https://goo.gl/KKBgku)| XB1 LFS
[](https://goo.gl/phJEXq)| PC LTS  | [](https://goo.gl/RuaG24)| PC LFS

##Spoiler Formatting

***For Spoiler Warning in Titles***
Begin your title with the tag "[Spoiler]".

---

**For Spoilers in Comments***
Format your comment like this:
**\[Spoiler: Who finally got a PS4?](#s "Norsefenrir! Happy birthday!")**
to have it displayed like this:
**[Spoiler: Who finally got a PS4?](#s "Norsefenrir! Happy birthday!")**

[ ](#/RES_SR_Config/NightModeCompatible)