Welcome to **[Fireteams](/r/Fireteams)**

>* **Useful Links**
 * [Public Event Schedule](http://goo.gl/bDsd36)
 * [Destiny Status](http://goo.gl/45by90)
 * [Destiny Ghost Hunter](http://goo.gl/y0zY7Z)
 * [Destiny Legacy](https://www.reddit.com/r/destinylegacy/)
>* **Reddit Groups**
 * [Reddit](http://goo.gl/iMiQ5B)
 * [Xbox 360](http://goo.gl/nKezwP)
 * [Xbox One](http://goo.gl/2Tpm2T)
 * [PS3](http://goo.gl/AeM6Yb)
 * [PS4](http://goo.gl/K4WcpD)

######**Still playing on Xbox360/PS3? While Legacy LFG posts are permitted here, perhaps r/destinylegacy can help you out!**

***

[**Dark Mode** *The Darkness Consumed You*](https://dm.reddit.com/r/DestinyTheGame "Switch mode")

[**Light Mode** *Become a Guardian of the Light*](https://www.reddit.com/r/DestinyTheGame "Switch mode")

***

## **Community Links**

* [Destiny Sherpas](http://goo.gl/OtoFXb)
* [Crucible Sherpas](https://goo.gl/HzAF1X)
* [Destiny Tech Support](https://goo.gl/iaaibC)
* [Destiny Discord](https://discord.gg/DestinyReddit)
* [Bungie](http://www.bungie.net/)

---

# [Farm Rules](https://goo.gl/nzE7GQ)

1.  Keep it civil, stay on topic, and follow [redditquette](http://www.reddit.com/wiki/reddiquette). No clowning around, period. Including but not limited to inside jokes with "friends".

2. No witch-hunting or harassment of any kind.

3. No buying, selling, trading, begging, or giving away of anything. Period.

4. Don't spam. No Twitch/YouTube/Etc promotion. Any type of promotion that is allowed should be thoughtful, limited, and well received.

5. If you are recruiting for a clan, please limit clan posts to at most once a week in the main sub and also once a week in the Weekly Clan Recruitment Post. Only links permitted are to Bungie.net or a application form. Links like youtube/twitter/twitch/discord are not permitted and can only be PM'd to interested candidates. You are welcome to reply to those who may fit into your clan, but please don't reply to everyone in the thread and actually pick those that are relevant.

---

> ##Filters
[](https://goo.gl/psKDPX)| Xbox One | [](https://goo.gl/7SpSjK)| PS4
-|-|-|-
[](https://goo.gl/WW97SZ)| PC| 


##Spoiler Formatting

***For Spoiler Warning in Titles***
Begin your title with the tag "[Spoiler]".

---

**For Spoilers in Comments***
Format your comment like this:
**\[Spoiler: Who finally got a PS4?](#s "Norsefenrir! Happy birthday!")**
to have it displayed like this:
**[Spoiler: Who finally got a PS4?](#s "Norsefenrir! Happy birthday!")**

[ ](#/RES_SR_Config/NightModeCompatible)